#
# Be sure to run `pod lib lint talk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'talk'
  s.version          = '0.1.0'
  s.summary          = 'A short description of talk.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/talk/talk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'disidea' => 'ios.7@qq.com' }
  s.source           = { :git => 'https://github.com/talk/talk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'talk/Classes/**/*'
  
  # s.resource_bundles = {
  #   'talk' => ['talk/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'Accelerate', 'AudioToolbox', 'AVFoundation', 'CFNetwork', 'CoreLocation', 'CoreTelephony', 'GLKit', 'SystemConfiguration'
   s.library = 'c++', 'sqlite3.0', 'z.1.2.5', 'iconv.2.4.0'
#   , 'stdc++.6.0.9'
  # s.dependency 'AFNetworking', '~> 2.3'
end
