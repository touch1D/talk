# talk

[![CI Status](https://img.shields.io/travis/talk/talk.svg?style=flat)](https://travis-ci.org/talk/talk)
[![Version](https://img.shields.io/cocoapods/v/talk.svg?style=flat)](https://cocoapods.org/pods/talk)
[![License](https://img.shields.io/cocoapods/l/talk.svg?style=flat)](https://cocoapods.org/pods/talk)
[![Platform](https://img.shields.io/cocoapods/p/talk.svg?style=flat)](https://cocoapods.org/pods/talk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

talk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'talk'
```

## Author

disidea, ios.7@qq.com

## License

talk is available under the MIT license. See the LICENSE file for more info.
