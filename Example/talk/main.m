//
//  main.m
//  talk
//
//  Created by talk on 12/20/2021.
//  Copyright (c) 2021 talk. All rights reserved.
//

@import UIKit;
#import "LCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LCAppDelegate class]));
    }
}
