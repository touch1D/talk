//
//  LCAppDelegate.h
//  talk
//
//  Created by talk on 12/20/2021.
//  Copyright (c) 2021 talk. All rights reserved.
//

@import UIKit;

@interface LCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
